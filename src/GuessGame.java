
/**
 * Created by User on 18.09.2017.
 */
public class GuessGame {

    Player p1 = new Player();
    Player p2 = new Player();
    Player p3 = new Player();

    public void startGame() {
        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;
        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Загаданное число между 0 и 9...");
        while(true) {
            
            p1.guess();
            p2.guess();
            p3.guess();

            guessp1 = p1.number;
            System.out.println("Первый игрок думает, что это число " + guessp1);
            guessp2 = p2.number;
            System.out.println("Второй игрок думает, что это число " + guessp2);
            guessp3 = p3.number;
            System.out.println("Третий игрок думает, что это число " + guessp3);

            if (guessp1 == targetNumber) {
                p1isRight = true;
            }
            if (guessp2 == targetNumber) {
                p2isRight = true;
            }
            if (guessp3 == targetNumber) {
                p3isRight = true;
            }

            if (p1isRight || p2isRight || p3isRight)
            {
                System.out.println("У нас есть победитель!");
                System.out.println("Первый игрок угадал? " + p1isRight);
                System.out.println("Наверное второй игрок угадал? " + p2isRight);
                System.out.println("А может третий игрок угадал?" + p3isRight);
                System.out.println("Игра окончена");
                break;
            }
            else
            {
                System.out.println("Не кто не угадал,загадывайте ещё раз.");
            }
        }
    }
}


